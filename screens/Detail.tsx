import { View, Text, StyleSheet, Image } from 'react-native'
import React, { useState } from 'react';
export interface userType {
  id: number,
  fnameTH: string,
  lnameTH: string,
  fnameEN: string,
  lnameEN: string,
  nnameTH: string,
  nnameEN: string,
  birthdate: string,
  province: string,
  model: string,
  height: number,
  avatar: string
}
const Detail = ({ route }: { route: any }) => {
  const [user, _] = useState<userType>(route.params as userType);

  return (
    <View>
      <Image
        style={styles.coverImage}
        source={{
          uri: user.avatar
        }}
      />
      <View style={styles.textBox}>
        <Text><Text style={styles.textHeader}>FullName: </Text>{user.fnameEN} {user.lnameEN}</Text>
        <Text><Text style={styles.textHeader}>NickName: </Text>{user.nnameEN}</Text>
        <Text><Text style={styles.textHeader}>Birthdate: </Text>{user.birthdate}</Text>
        <Text><Text style={styles.textHeader}>Province: </Text>{user.province}</Text>
        <Text><Text style={styles.textHeader}>Model: </Text>{user.model}</Text>
        <Text><Text style={styles.textHeader}>Height: </Text>{user.height}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  coverImage: {
    width: '100%',
    height: 300
  },
  textBox: {
    width: '100%',
    height: 300,
    margin: 5
  },
  textHeader: {
    fontWeight: 'bold'
  }
});

export default Detail