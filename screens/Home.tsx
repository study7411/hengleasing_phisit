import { View, Text, ScrollView, StyleSheet, Image, Pressable, FlatList } from 'react-native';
import React, { useEffect, useState } from 'react';
import { ToggleButton, Searchbar, IconButton } from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import users from '../data/users.json';
export interface userType {
  id: number,
  fnameTH: string,
  lnameTH: string,
  fnameEN: string,
  lnameEN: string,
  nnameTH: string,
  nnameEN: string,
  birthdate: string,
  province: string,
  model: string,
  height: number,
  avatar: string
}
const Home = ({ navigation }: { navigation: any }) => {
  const [usersFilter, setUsersFilter] = useState<userType[]>([]);
  const [province, setProvince] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [order, setOrder] = useState<string>('asc');
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    { label: 'ชื่อ-นามสกุลไทย', value: 'fullnameTH' },
    { label: 'ชื่อ-นามสกุลอังกฤษ', value: 'fullnameEN' },
    { label: 'ชื่อเล่นไทย', value: 'nnameTH' },
    { label: 'ชื่อเล่นอังกฤษ', value: 'nnameEN' },
    { label: 'วันเกิด', value: 'birthdate' },
    { label: 'รุ่น', value: 'model' },
    { label: 'ส่วนสูง', value: 'height' }
  ]);

  useEffect(() => {
    if (users.length > 0) {
      let newUsers: userType[] = [];
      if (province == 'bangkok') {
        newUsers = (users.filter((u) => (
          u.province.toLowerCase() == province.toLowerCase()
        )));
      } else if (province != '') {
        newUsers = (users.filter((u) => (
          u.province.toLowerCase() != 'bangkok'
        )));
      } else {
        newUsers = users.map(u => u);
      }
      if (searchQuery > '') {
        newUsers = (newUsers.filter((u) => (
          u.fnameTH.toLowerCase().includes(searchQuery.toLowerCase()) ||
          u.lnameTH.toLowerCase().includes(searchQuery.toLowerCase()) ||
          u.fnameEN.toLowerCase().includes(searchQuery.toLowerCase()) ||
          u.lnameEN.toLowerCase().includes(searchQuery.toLowerCase())
        )));
      }
      if (value) {
        const sort = (order == 'asc') ? [1, -1] : [-1, 1];
        switch (value) {
          case 'fullnameTH':
            newUsers = newUsers.sort((a, b) => {
              a.lnameTH.localeCompare(b.lnameTH)
              return (a.fnameTH > b.fnameTH) ? sort[0] : sort[1];
            }).map(u => u);
            break;
          case 'fullnameEN':
            newUsers = newUsers.sort((a, b) => {
              a.lnameEN.localeCompare(b.lnameEN)
              return (a.fnameEN > b.fnameEN) ? sort[0] : sort[1];
            }).map(u => u);
            break;
          case 'nnameTH': case 'nnameEN': case 'birthdate': case 'model': case 'height':
            newUsers = newUsers.sort((a, b) => (a[value] > b[value]) ? sort[0] : sort[1]).map(u => u);
            break;
          default:
            break;
        }
      }
      setUsersFilter(newUsers);
    }
  }, [searchQuery, province, value, order]);

  const pressDetail = (user: userType) => {
    navigation.navigate('Detail', user);
  }

  const onChangeSearch = (query: React.SetStateAction<string>) => setSearchQuery(query);
  return (
    <View style={{ alignItems: 'center' }}>
      <Searchbar
        placeholder='Search'
        onChangeText={onChangeSearch}
        value={searchQuery}
      />
      <ToggleButton.Row onValueChange={province => setProvince(province)} value={province}>
        <ToggleButton style={{ width: '33%' }} icon={() => <View><Text style={{ color: 'black' }}>All</Text></View>} value='' />
        <ToggleButton style={{ width: '33%' }} icon={() => <View><Text style={{ color: 'black' }}>Bangkok</Text></View>} value='bangkok' />
        <ToggleButton style={{ width: '33%' }} icon={() => <View><Text style={{ color: 'black' }}>Other</Text></View>} value='other' />
      </ToggleButton.Row>
      <View style={{ zIndex: 2000, flexDirection: 'row' }}>
        <DropDownPicker
          open={open}
          value={value}
          items={items}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setItems}
          maxHeight={300}
          containerStyle={{ width: '90%' }}
          style={{ borderColor: 'grey' }}
        />
        <IconButton
          icon={() => <View><Text style={{ color: 'black' }}>{(order == 'asc') ? '⬇️' : '⬆️'}</Text></View>}
          size={20}
          onPress={() => setOrder((order == 'asc') ? 'desc' : 'asc')}
          style={{ width: '6%' }}
        />
      </View>
      <View style={{ zIndex: 1000 }}>
        <ScrollView style={{ maxHeight: '85%' }}>
          {usersFilter.map(u => (
            <Pressable key={u.id} onPress={() => pressDetail(u)}>
              <View
                style={{
                  flexDirection: 'row'
                }}>
                <Image
                  style={styles.coverImage}
                  source={{
                    uri: u.avatar
                  }}
                />
                <View style={styles.textBox}>
                  <Text><Text style={styles.textHeader}>ชื่อจริง: </Text>{u.fnameTH} {u.lnameTH}</Text>
                  <Text><Text style={styles.textHeader}>ชื่อเล่น: </Text>{u.nnameTH}</Text>
                </View>
              </View>
            </Pressable>
          ))}
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  coverImage: {
    width: '40%',
    height: 100
  },
  textBox: {
    width: '60%',
    height: 100,
    margin: 5
  },
  textHeader: {
    fontWeight: 'bold'
  }
});

export default Home;