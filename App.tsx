import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './screens/Home';
import Detail from './screens/Detail';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{ title: 'Members' }}
        />
        <Stack.Screen
          name="Detail"
          component={Detail}
          options={{ title: 'Detail'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
